\section{Introduction}

Wearable devices can convey information to users via different sensory modalities.
Visual and auditory interfaces are already used on numerous devices, including smart watches, activity trackers, and headsets.
Recently, there has been increased interest in further developing haptic interfaces, i.e., those that use vibrations, pressure, or temperature as signals.
Haptic information has several advantages: it can be conveyed privately, in an eyes-free manner, and during physical activity~\cite{Roumen2015}.

Despite the increasing interest in haptic information transmission, the modality is far less studied than its visual and auditory counterparts~\cite{Craig1995}. For example, the effects of brief distracting events have been extensively characterized in the visual and auditory domains, yet haptic susceptibility to such distraction remains poorly understood. To make the best use of the haptic modality, such psychological features need to be characterized and then used to inform design.

From a psychological perspective, a sensory event or stimulus is distracting when it captures a person's attention to the detriment of other tasks~\cite{Egeth1997}. For example, a ringing phone may transiently disrupt reading a report. Note that such distraction requires neither perceptual confusion nor masking; basic visual processing is unimpaired by a distracting sound, but attention is redirected away from the visual task to deal with the distracting event or stimulus~\cite{Chun2002}. Impairment can occur even if the person does not look away~\cite{Asplund2010}. Of course, he or she often will.

In addition to intensity, several factors influence whether a stimulus is distracting. These include:


\begin{enumerate}
	\item Expectation. Relatively rare or unpredictable events capture attention~\cite{Egeth1997,Squires1975,Katayama1998,Yamaguchi1991}. Although alerts or notifications are meant to grab attention, there is a cost: The ongoing task is disrupted, leading to slowing or errors~\cite{Chun2002,Asplund2010,Horstmann2015,Vachon2017,Parmentier2011}. In an information theory sense, rare and unpredictable events are surprising~\cite{Mars2008,Horstmann2015}, and their effects are termed "surprise capture". Accordingly, if rare events come to be expected, they become less surprising and therefore less distracting.

	\item Relevance. Events that are relevant to the current task or to broader goals tend to capture attention. For example, if one is waiting for a notification sound, similar sounds will grab one's attention. Such effects are termed "contingent capture", and they have also been shown to disrupt ongoing tasks~\cite{Folk1992,Folk2002}.

	\item Modality. Attention can be captured across modalities (e.g. auditory alerts disrupting visual processing), but distraction tends to be stronger within a given modality (e.g. auditory distraction on an auditory task)~\cite{Ljungberg2012}.

	\item Timing. Distracting effects evolve over time. Indeed, brief distracting events (tens to hundreds of ms) tend to produce powerful yet transient disruptions, which themselves last under a second~\cite{Asplund2010,Horstmann2015,Ljungberg2012}.
\end{enumerate}
These distraction "principles" have been formulated primarily from auditory and visual experimentation, but distracting events may affect haptic tasks in similar ways. Environmental sounds or vibrations (e.g. the "phantom vibrations" of a mobile phone) could be disruptively distracting, as could alerts or notifications that are intended to be helpful~\cite{Egeth1997}. Such alerts can improve performance on a variety of tasks~\cite{Baldwin2012,Merlo2011,Chen2008,Stanley2006,Kochhar2012,Marsalia2016}, but a poorly-timed or overly distracting alert may disrupt processing of the very information to which it was intended to direct attention. Alerts in each sensory domain--including auditory, visual, and haptic--will be experienced more frequently as devices employing them becoming increasingly common.

In this study, we assess the effects of distracting stimuli on two vibrotactile tasks. Each task involves either target detection or pattern discrimination on the fingers, as haptic sensitivity is greatest there~\cite{Cholewiak1984} and smart rings have been developed accordingly. Across four experiments, we explore how these tasks can be disrupted by relatively rare "surprise" distractors, including changes in distraction effects over successive presentations (\textit{Expectation}, see list of factors above). The surprise distractors are not relevant to the primary tasks themselves (\textit{Relevance}), though they have the same modality as the targets (vibrations) in two experiments and have a different modality (sounds) in two others (\textit{Modality}). Finally, we explore the timecourse of distracting effects by varying the time between the surprise distractors and targets (\textit{Timing}).

Our experiments assess the potential importance of attentional capture effects on the detection of vibrotactile information. As such, the study is novel and informative for basic psychology as well as for interactions with haptic devices.

The contribution of this paper is three-fold. Specifically, it provides the following:
\begin{enumerate}
    \item A succinct review of psychological and HCI considerations for distracting events and beneficial alerts. Our focus is on situations with haptic components, though the principles are drawn from and generalize to other sensory modalities.
    \item An empirical investigation of the effects of surprise distraction on vibrotactile task performance. Experiments 1A and 1B show that relatively unexpected vibrotactile and auditory events can impair the detection of a target vibration's location. Experiments 2A and 2B show that these same stimuli harm vibrotactile pattern recognition. The effects of these distraction effects change over time, with strong effects for several hundred milliseconds that then diminish--potentially even becoming beneficial alerts after roughly a second.
    \item A set of design guidelines for avoiding the pitfalls of attentional capture effects on vibrotactile tasks. We also discuss how smart ring systems could be engineered to present information at ideal timings relative to other stimuli.
\end{enumerate}
