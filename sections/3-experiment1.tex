\section{Experiments 1A and 1B: Effects of Distraction on Vibrotactile Location Detection}
In this first pair of experiments, we sought to understand how a relatively rare and unexpected stimulus (the surprise distractor) would affect the detection of a specific vibrotactile target presented on one of four fingers. We expected that the surprise distractor would have detrimental effects on target detection due to attentional capture~\cite{Parmentier2011,Ljungberg2012,Yamaguchi1991}. These effects could reduce (habituate) with repeated presentations as the surprise distractor became more familiar. Indeed, a surprise distractor could even have facilitatory effects if participants learned that it acts as a temporal cue, predicting target onset~\cite{Parmentier2011,Marsalia2016}.

We designed our task to represent a challenging perceptual scenario. Target detection was made difficult through the choice of stimuli, which were short pulses on different fingers without an intervening pause~\cite{Craig1995, Hillstrom2002}. In Experiment 1A, we used a brief, wrist-based vibration as the Surprise distractor. In Experiment 1B, we used a sound. Otherwise, the experiments contained virtually identical apparati, procedures, tasks, and stimuli.

\subsection{Common Apparatus}
An adjustable ring-like device was attached to the middle phalanx of each finger of the dominant hand (excluding the thumb). Using the middle phalanx allowed for more spatial separation between the devices, and the middle and proximal phalanges have similar sensitivity~\cite{Weinstein1962}. (Nevertheless, note that we used the proximal phalanx, a more natural location for smart ring placement, in Experiment 2.) Each device contained a small, coin-type vibration motor. In Experiment 1A, an additional motor was attached to the wrist (Figure~\ref{fig:1a-apparatus}). All motors were attached to the same hand because same-hand interference is better characterized in the tactile literature~\cite{Craig1995,Hillstrom2002}, and we sought to test for potential interference.
Each vibration motor had a typical operating amplitude of 1.3 G (Precision Microdrives 310-103; data sheet available at \href{https://www.precisionmicrodrives.com/product/datasheet/310-103-10mm-vibration-motor-3mm-type-datasheet.pdf}{https://www.precisionmicrodrives.com/product/datasheet/310-103-10mm-vibration-motor-3mm-type-datasheet.pdf}). The motors were controlled by an Arduino Uno microcontroller. As the Arduino provided 5 V with a maximum current of 40 mA, the motors were driven outside their rated typical values of 3 V and 58 mA. We therefore tested the motors with our setup to record their frequencies and relative amplitudes for each experiment (see the Common Task and Stimuli sections for both Experiment 1 and Experiment 2).

The Arduino Uno itself was controlled by a 2013 iMac running our Python-based experimental software. Participant responses were recorded through a standard USB keyboard. Participants rested the fingers of their dominant hand on four response keys (index finger on F, middle finger on G, ring finger on H, and small finger on J; reversed for left handers). This setup meant that participants indicated the target vibration location by using the stimulated finger itself, thereby reducing confusion; the raised bumps on the F and J keys helped participants keep their hands on the appropriate set of keys (see additional notes on this design choice in the Experiment 1B introduction below). The response hand was covered by a box so that motor vibration was not visible. Participants also wore Sony MDR-ZX110 over-ear headphones connected to the computer's 3.5mm headphone jack. Participants were told that the purpose of the headphones was to block the noise of the vibrating motors, which was one of their primary functions. No sound was played through the headphones, save the auditory surprise distractors in Experiment 1B.
\begin{figure}[!]
	\centering
  \includegraphics[width=.80\columnwidth]{figures/expe1a-apparatus}
  \caption{%
    Apparatus used in Experiment 1A, with four motors on the fingers and one on the wrist. In Experiment 1B, the wrist motor was not attached.
    \label{fig:1a-apparatus}%
  }
\end{figure}

\subsection{Common Procedure}
Before the experiment began, participants were briefed on the task and provided informed consent. The consent procedure and protocols for all four reported experiments were approved by the local IRB.
After wearing the apparatus, each participant was trained in its use. The experimenter generated a stimulus and then asked the participant to identify on which finger the target was presented.
The experiment was divided into 7 blocks, each with 24 trials.
The first block was for practice and contained no surprise stimuli.
In each of the 6 subsequent blocks, 4 trials (16.67\% of the total) contained an unexpected alert (surprise stimulus).
The rarity of the surprise, as well as its timing relative to the target, was chosen based on previous literature~\cite{Asplund2010,Horstmann2015,Folk2002,Vachon2017,Yamaguchi1991}.
Participants were encouraged to take breaks between blocks and completed the experiment in approximately 30 minutes. Participants were compensated with either payment (3.7 USD) or optional research participation credit for a psychology course.

\subsection{Common Task and Stimuli}
Participants rested the fingers of their dominant hand on the four response keys. They then initiated each trial by pressing the keyboard's space key. A sequence of eight 200 ms vibrational pulses followed. The location (finger) of each pulse was random, with the restriction that a finger would not receive two sequential pulses.
One of the eight pulses (the target, item 3-7 in the sequence) was driven with a pulse width modulation (PWM) value of 100\%, whereas the other pulses were driven at PWM 40\%. Stimulus measurements confirmed that the relative vibration amplitudes were roughly as intended, with 40\% PWM stimulus amplitude approximately 35\% of the 100\% PWM stimuli. The 100\% PWM stimuli (targets and surprise distractors) vibrated at approximately 130 Hz, a frequency at which the motor would produce minimal bone conductance or direct auditory perception~\cite{Mcbride2017}. The 40\% PWM stimuli vibrated at approximately 40 Hz. Although bone conductance is more likely at this frequency, neither the target's location nor occurence would be revealed by such stimuli. We selected the PWM settings and vibration durations following pilot sessions, choosing values that provided a challenging yet feasible task. Participants indicated the location (finger) of each trial's target vibration using the stimulated finger itself.
When present, the surprise distractor began 350 milliseconds before the target vibration. At least two trials without a surprise distractor (target-only trials) occurred between any two trials with a surprise distractor (labelled as "surprise trials").

\subsection{Common Design}
A $4 \times 2$ within-subject design was used with three independent variables, including a continuous one: \textit{Trial Type \{Target Only, Surprise\}}, \textit{Finger \{ Index, Middle, Ring, Pinky\}}, and \textit{Session Time \{trial position within the session\}}. For visualization puposes, we binned Session Time into three periods (Early: Blocks 2 and 3, Middle: Blocks 4 and 5, Late: Blocks 6 and 7). Trial Type and Finger were fully crossed and randomized within each Block. As such, each participant experienced 4 fingers $\times$ [1 (training block) + 6 (test blocks)] $\times$ 6 repetitions = 168 trials, including 24 trials with a surprise distractor.
Our dependent variable was accuracy. A trial's response was considered successful if the participant was able to correctly identify on which finger the target vibration occurred.

\subsection{Common Analytical Approach}
Data preparation, statistical analysis, and visualizations were all implemented in RStudio version 1.0.136 (R Foundation for Statistical Computing) running R version 3.2.4. For descriptive statistics, we report marginal means and standard deviations or standard errors of the mean. Before calculating descriptive and inferential statistics for the sample, we removed participants whose detection performance in target-only trials was not reliably above chance performance, a procedure used in similar studies~\cite{Murphy2016,Murphy2018,Obana2019}. We reasoned that we could not assess the effects of surprise distractors on performance from such individuals. Data from participants whose performance was below 40\% correct detection on any finger were removed from the sample. Given that targets were delivered to each finger in 36 target-only trials per participant, the 40\% cutoff identifies performance significantly above chance (25\%) based on proportion tests. Furthermore, we examined the pattern of errant responses to ensure that low participant performance was not due to swapped motor positions (experimenter error) or consistent perceptual or response biases (e.g. consistently reporting index finger stimulation as middle finger stimulation). We found only one such case, which is described in Experiment 1B.

Given each trial's binary detection outcome, we constructed generalized linear mixed-effect models for our primary analyses. The factors were Trial Type (2 levels; surprise trial or target-only trial), Session Time (continuous), and Finger (4 levels, one per finger location of the target). By-subject random slopes and intercepts for the categorical factors (Trial Type and Finger) were included in the error terms~\cite{Barr2013}. The resulting logistic regression models were fit using glmer() in the lme4 package (version 1.1-17)~\cite{Bates2015}. They were then assessed using the Anova() function in the car package (version 3.0-2), which generates tables of Type II Wald chi-squared tests~\cite{Fox2011}.
Follow-up pairwise tests were conducted on the estimated marginal means using emmeans() in the emmeans package (version 1.3.1). In addition, we conducted a secondary analysis on the types of errors made, either those to an adjacent finger (near) or another finger (far). As the dependent measure for this analysis was response rate (continuous), we constructed linear mixed-effect models with factors of Trial Type and Error Distance (near or far). The models were fit using lmer() in the lme4 package. To control for multiple comparisons, false discovery rate (FDR) correction was applied to all p-values in the study~\cite{Benjamini1995}.

\section{Experiment 1A: Effects of Vibrotactile Surprise Distractors on Target Detection}
In the first experiment, the relatively rare and unexpected stimulus (the surprise distractor) was a single 200 millisecond vibration (100\% PWM) presented on the back of the wrist, mimicing the placement of a smartwatch.
The surprise distractor had the same duration as all other vibration pulses, as well as the same intensity as the target vibration.
The apparatus, procedure, task, stimuli, and design were described above.

\subsection{Participants}
Forty-two participants (27 female, 39 right-handed), aged 18-32 ($M=22.6$) took part in the experiment.
Owing to apparatus failures or experimenter error, the data from 10 participants could not be used. In addition, eight participants were excluded for failing to achieve at least 40\% target detection for each finger.
The final sample included 24 participants $\times$ 168 trials = 4,032 trials, including 576 with a surprise distractor.

\subsection{Results}
After the data from Block 1 (practice) had been discarded, we visualized the results in Figure~\ref{fig:1a-results}.

\paragraph{Trial Type.}
Although target detection performance was high, unexpected wrist vibrations substantially impaired performance (main effect of Trial Type: $\chi^{2}(1)=59.8, p<.001$). Participants successfully detected the target more often without the Surprise distractor ($M=84.7\%$) than with it ($M=71.4\%$).
\begin{figure}[!]
	\centering
  \includegraphics[width=.50\columnwidth]{figures/expe1a-results}
  \caption{%
    Recognition rate for Experiment 1A. Error bars represent the standard error of the mean. Each value for time during session represents two blocks, the training block is not counted.
    \label{fig:1a-results}%
  }
\end{figure}

\paragraph{Session Time.}
Performance also improved over time (main effect of Session Time: $\chi^{2}(1)=11.9, p=.001$).
Performance on target-only trials rose from 82.7\% during the early part of the experiment to 86.1\% during the late part, whereas accuracy rose from 63.5\% to 75.5\% for surprise trials.

\paragraph{Finger.}
We found a significant main effect of Finger on accuracy ($\chi^{2}(3)=25.6, p<.001$). To better understand this effect, we constructed confusion matrices (Figure~\ref{fig:1a-confusion}), which revealed participants' error types. Most errors involved spatial confusion, in which the finger adjacent to the target location was selected ("near" errors). Note that if errors were random, one would expect an approximately equal frequency of "near" and "far" errors; six cells of the error matrix contribute to each error type. A follow-up linear mixed-effects model (see above) revealed significant main effects of both Trial Type ($\chi^{2}(1)=42.8, p<.001$) and Error Distance ($\chi^{2}(1)=24.9, p<.001$), with no significant interaction ($\chi^{2}(1)=0.002, p=.98$). The presence of a surprise distractor increased both types of errors by approximately the same amount. Near errors increased from 11.7\% to 19.3\%, whereas far errors increased from 3.5\% to 9.4\%. These results indicate that the surprise distractors primarily induced more random guessing, not increased spatial confusion. We suggest that during most surprise trials, the target was missed completely, not merely degraded~\cite{Asplund2014,Sergent2004}.

\begin{figure}[!]
	\centering
  \includegraphics[width=.80\columnwidth]{figures/expe1a-confusion}
  \caption{%
    Confusion matrices for Experiment 1A. Stimuli are columns, responses rows. Accuracy is in \%.
		The $\frac{\sum}{4}$ column shows how frequently each response was chosen. Equally distributed answers have a value of 25.
		Higher values suggests that this answer is chosen more often. Finger 2 is index finger, 3 is middle finger, 4 is ring, 5 is pinky.
    \label{fig:1a-confusion}%
  }
\end{figure}

\paragraph{Interactions.}
We found a significant Trial Type $\times$ Finger interaction ($\chi^{2}(1)=12.8, p=.010$). During target-only trials, participants were more accurate with their index ($M=88.8\%$) and middle finger ($M=92.6\%$) as compared to their ring ($M=79.9\%$) and pinky ($M=77.6\%$). Post-hoc pairwise tests confirmed these performance scores were significantly different from one another ($p<.003$), save ring versus pinky ($p=.28$). Performance was also significantly different across finger pairs during surprise trials (all $p<.01$), save index versus ring ($p=.87$). Detection problems were particularly acute on the pinky ($M=48.6\%$), with 33.3\% of targets delivered to it reported as ring finger stimulation. Finally, significant impairments due to the surprise distractors were found on all fingers ($p<.035$), with the exception of the middle one ($p=.49$). The middle finger received more responses overall during surprise trials, so response bias could account for the null effect.
No other interactions were significant (Trial Type $\times$ Session Time: $\chi^{2}(1)=2.45, p=.17$; Session Time $\times$ Finger: $\chi^{2}(3)=5.59, p=.19$; Trial Type $\times$ Session Time $\times$ Finger: $\chi^{2}(3)=5.90, p=.17$).

\subsection{Discussion}
Our results suggest that unexpected vibrotactile stimuli have an impact on user's ability to perform the task, with an average decrease in accuracy of 13.3 percentage points. Importantly, this deficit is fairly consistent over time, with a 10.6 points effect still present late in the session. Moreover, despite repeated exposure to the surprise distractor and its consistent prediction of the target's arrival, participants did not use the alert to improve their performance.
Our experiment also highlights perception differences between the fingers, as we observed an effect of Finger on accuracy. Performance on the index and middle finger was consistently better, even during surprise stimulus presentation. In contrast, target detection on the pinky began lower during target-only trials (77.6\%) and was further reduced during surprise trials (48.6\%). Although the overall performance differences across the fingers are clear, the additional deficits caused by surprise distraction are more difficult to interpret. We consider these ideas further in the General Discussion.


\section{Experiment 1B: Effects of Auditory Surprise Distractors on Target Detection}
In Experiment 1A, we showed that a relatively rare and unexpected vibrotactile stimulus could disrupt target detection, even though the surprise distractor was separately both spatially and temporally from the target. The shared modality (as well as shared features) of the target and surprise distractor may have rendered participants unable to ignore the surprise distractor. To investigate whether disruption could occur even across modalities, we investigated the effects of unexpected auditory distractors on vibrotactile target detection. Such distraction is ecologically relevant, as we are bombarded with both relevant and irrelevant noises in our daily life, ranging from conversations to chirping birds to device notifications.

For Experiment 1B, we used a set of such sounds that have been shown to cause distraction to the detriment of auditory task performance~\cite{Obana2019}. These sounds included spoken letters, environmental noises, and synthetically produced pitches. Each sound was used as a surprise distractor in a given surprise trial, with the sound presented through headphones 350 milliseconds before the target vibration. For each surprise trial, the surprise distractor was randomly drawn from the set of 80 sounds. Changing the surprise distractor has been found to increase the longevity of the auditory effects across successive surprise trials (Surprise-induced Deafness), with few apparent differences in the effects across the various sound types~\cite{Obana2019}. All sounds were compressed to 110 milliseconds and normalized to ensure a comparable energy profile~\cite{Font2013}. Stimulus measurements confirmed the roughly equal volumes, which ranged between 60 and 70 dB~\cite{Shen2006,Obana2019}. The apparatus, procedure, task, stimuli, and design were otherwise as described above.

Second, the first six participants in the sample responded with their non-dominant hand, while their dominant hand received the vibrotactile stimulation. Although instructed to respond with the corresponding finger (e.g. left index for right index stimulation), we found that 3 of 6 participants had consistently reversed their mappings (based both on data analysis and participant reports). These individuals had instead responded with corresponding spatial positions (e.g. left pinky, the rightmost finger of the left hand, for right index stimulation). To ensure more consistent performace with reduced input errors, we adjusted the procedures so that the dominant hand was used for both stimulation and response. As baseline task performance (after correction, when applicable) for these first six participants was qualitatively similar to the remainder of the sample, we included them in the final sample. Their inclusion or exclusion did not affect the final results or our conclusions. Finally, as Experiment 1A's data collection actually started slightly after Experiment 1B's began, we could use the improved procedure throughout.

\subsection{Participants}
Twenty-seven participants (16 female, 25 right-handed), aged 18-29 ($M=21.5$) took part in this experiment. The finger-bands for one left-handed participant were errantly reversed; the data from this participant were included after correction. Seven participants were excluded from the final sample for failing to achieve at least 40\% target detection on one or more fingers, performance that was due neither to errant motor placement nor consistent perceptual biases.
The final sample included 20 partipants $\times$ 168 trials = 3,360 trials, including 480 with a surprise stimulus.

\subsection{Results}
After the data from Block 1 (practice) had been discarded, we visualized the results in Figure~\ref{fig:1b-results}.

\paragraph{Trial Type.}
Target detection performance during target-only trials was high ($M=86.3\%$), and surprise distractors impaired performance ($M=81.5\%$). The impairment was numerically slight, albeit still significant (main effect of Trial Type: $\chi^{2}(1)=9.51, p=.004$).
\begin{figure}[!]
	\centering
  \includegraphics[width=.50\columnwidth]{figures/expe1b-results}
  \caption{%
    Recognition rate for Experiment 1B. Error bars represent the standard error of the mean. Each value for time during the session represents two blocks.
    \label{fig:1b-results}%
  }
\end{figure}

\paragraph{Session Time.}
Performance did not significantly vary across the session (main effect of Session Time: $\chi^{2}(1)=1.10, p=.37$).

\paragraph{Finger.}
Performance did not significantly vary across fingers (main effect of Finger: $\chi^{2}(3)=6.25, p=.16$).
Averaged across all trials (target-only and surprise), performance ranged from 90.5\% for the index to 79.9\% for the pinky. Individual finger performance is reported in Figure~\ref{fig:1b-confusion}. Consistent with Expt. 1A, most errors involved spatial confusion ("near" errors). A follow-up linear mixed-effects model (see above) revealed significant main effects of both Trial Type ($\chi^{2}(1)=7.99, p=.009$) and Error Distance ($\chi^{2}(1)=5.99, p=.026$), with no significant interaction ($\chi^{2}(1)=0.00, p=.99$). As before, the presence of a surprise distractor increased both types of errors. Near errors increased from 9.8\% to 13.1\%, whereas far errors increased from 3.9\% to 5.4\%.

\begin{figure}[!]
	\centering
  \includegraphics[width=.80\columnwidth]{figures/expe1b-confusion}
  \caption{%
    Confusion matrices for Experiment 1B. Stimuli are columns, responses rows. Accuracy is reported as a percentage. Finger 2 is index finger, 3 is middle finger, 4 is ring, 5 is pinky.
    \label{fig:1b-confusion}%
  }
\end{figure}

\paragraph{Interactions.}
We did not find any significant interactions in this experiment (all $p>.23$).

\subsection{Discussion}
\paragraph{Experiment 1B}
Our auditory surprise distractors impaired performance, with a small but statistically significant loss of accuracy (4.9 percentage points) that appeared to be stable across time.
Performance differences across individual fingers showed a pattern similar to Experiment 1A, though neither these differences nor the interaction with Trial Type were significant. Although these null results could reflect a true absence of such effects, they could represent a lack of power for detecting more small-to-moderate differences. We consider this issue further in the General Discussion.

Experiment 1B contained two other limitations worth noting, and we addressed each in the design for Experiment 2B. First, a different surprise distractor was used for each trial, which may have contributed to the persistent target detection deficit. Second, in subsequent testing for a different study with a similar apparatus, we found that participants reported that a vibrotactile and an auditory stimulus began simultaneously when the software command to the vibration motor was issued approximately 33 ms before its command to play a pure tone sound. This small timing error likely reflects both different hardware latencies and perceptual factors. Regardless, we still observed a target detection deficit, though its magnitude relative to Experiment 1A's may reflect differences other than modality alone.

Despite these limitations, the primary conclusion of the experiment is clear: Unexpected sounds can disrupt vibrotactile target detection. To avoid the deleterious effects of auditory surprise distraction, one could design vibrotactile systems that also detect sounds. The pattern's presentation could then be delayed in the presence of a potentially distracting sound; this strategy would increase accuracy at the cost of time. We further discuss this possibilty for system design in the General Discussion.

\paragraph{Experiments 1A and 1B}
Our first pair of experiments shows that a surprise distractor, a relatively rare and unexpected stimulus, can impair participants' ability to identify a subsequent target.
This effect was found for both vibrotactile and auditory surprise distractors. Although the vibrotactile surprise distractor was separated both spatially and temporally from the target, their shared modality likely contributed to the larger surprise-related impairment~\cite{Hillstrom2002,Soto2002,Dellacqua2006}.
Participants may also have unwittingly shifted their attentional focus to the wrist because the surprise's features (a relatively strong vibration) matched the target's, thereby leading to contingent attentional capture~\cite{Folk1992,Folk2002}.
Finally, the effect's persistence throughout each session suggests that participants do not--and perhaps cannot--ignore the surprises or use them as predictive cues of the target's imminent arrival.

Our experiments also show that vibrotactile target detection performance may differ across fingers.
Previous work suggests that spatial acuity and force detection is better on the index and middle fingers~\cite{VegaBermudez2001,King2010,Sathian1996}, which is consistent with the findings reported here.
In addition, surprise distractors may affect detection across the fingers differently, with large effects on the pinky and more moderate effects on the index and middle fingers. Such differences, however, may simply reflect baseline performance differences across the fingers.
Regardless, we suggest using the index and middle fingers for smart ring notification delivery.

These initial experiments also contained limitations, which we addressed in Experiments 2A and 2B. First, although pilot tests indicated that the headphones effectively blocked the sounds from the vibration motors, it is possible that they did not completely block such sounds for all participants. That said, it is unlikely that these sounds were the primary cause of the observed deficits. Unexpected vibrotactile distractors (Experiment 1A) caused worse target detection performance as compared to auditory distractors (Experiment 1B), despite the latter's greater volume. In Experiment 2, white noise was presented through the headphones. Second, in Experiment 1, the durations of the surprise distractors differed across the modalities, and auditory surprise distractors varied while only one vibrotactile surprise distractor was used. These differences were removed in Experiment 2, yet it is still challenging to draw strong conclusions about the relative magnitudes (and importance) of vibrotactile and auditory surprise distraction on target detection. Nevertheless, the presence of both types of deficits is important, as is their relative magnitude for addressing potential confounds (e.g. noise from the vibration motors). We further discuss comparisons across modalities in the Limitations section following the General Discussions.

In addition to addressing these limitations, the aim of Experiment 2 was to use a more HCI-relevant experimental design to explore the timing between the surprise distractor and its target. Characterizing this timing relationship is important for understanding the surprise distraction effects, as well as for designing systems that mitigate problematic attentional capture deficits, perhaps while also using alerting notifications effectively.